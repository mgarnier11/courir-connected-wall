import Firebase from 'firebase';

import * as uuid from 'uuid';

import { Injectable } from '@angular/core';
import { FirebaseService } from './firebase.service';
import { Module, ModuleErrors } from 'src/models/module';
import {
  ConnectedWall,
  ConnectedWallErrors,
  FirestoreConnectedWallConverter,
} from 'src/models/connectedWall';

@Injectable({
  providedIn: 'root',
})
export class ConnectedWallsService {
  public get connectedWallCollection() {
    return this._firebaseService.firestore
      .collection('connectedWalls')
      .withConverter(FirestoreConnectedWallConverter);
  }

  constructor(private _firebaseService: FirebaseService) {}

  public subscribeToConnectedWallChanges(
    connectedWallId: string,
    onUpdate: (newConnectedWall: ConnectedWall) => void,
    onError: (error?: Firebase.firestore.FirestoreError) => void
  ) {
    console.log('subscribed');

    return this.connectedWallCollection.doc(connectedWallId).onSnapshot(
      (doc) => {
        if (doc.exists) {
          onUpdate(doc.data()!);
        } else {
          onError();
        }
      },
      (err) => {
        onError(err);
      }
    );
  }

  public removeListenner() {}

  public async getConnectedWalls(): Promise<ConnectedWall[]> {
    const connectedWallDocs = (await this.connectedWallCollection.get()).docs;

    return connectedWallDocs.map((d) => d.data());
  }

  public async getConnectedWall(
    connectedWallId: string
  ): Promise<ConnectedWall> {
    const connectedWallDoc = await this.connectedWallCollection
      .doc(connectedWallId)
      .get();

    if (connectedWallDoc.exists) return connectedWallDoc.data()!;
    else throw ConnectedWallErrors.ConnectedWallNotFound;
  }

  public async createConnectedWall(
    modules: Module[],
    name: string
  ): Promise<ConnectedWall> {
    const newConnectedWallDoc = await this.connectedWallCollection.add({
      modules,
      name,
    } as ConnectedWall);

    return (await newConnectedWallDoc.get()).data()!;
  }

  public async updateConnectedWallName(
    connectedWallId: string,
    name: string
  ): Promise<ConnectedWall> {
    const connectedWallDoc = this.connectedWallCollection.doc(connectedWallId);

    const connectedWallRef = await connectedWallDoc.get();

    if (connectedWallRef.exists) {
      await connectedWallDoc.update({ name });

      return (await connectedWallDoc.get()).data()!;
    } else {
      throw ConnectedWallErrors.ConnectedWallNotFound;
    }
  }

  public async updateConnectedWallModule(
    connectedWallId: string,
    module: Module
  ): Promise<ConnectedWall> {
    // find the connectedWall to update using connectedWall Id
    // then searches if the module is present with module.id
    // if it is, updates it,
    // if not throw an error

    const connectedWallDoc = this.connectedWallCollection.doc(connectedWallId);

    const connectedWallRef = await connectedWallDoc.get();

    if (connectedWallRef.exists) {
      const connectedWall = connectedWallRef.data()!;

      const connectedWallModuleIndex = connectedWall.modules.findIndex(
        (m) => m.id === module.id
      );

      if (connectedWallModuleIndex > -1) {
        connectedWall.modules[connectedWallModuleIndex] = module;

        await connectedWallDoc.update({ modules: connectedWall.modules });

        return connectedWall;
      } else {
        throw ModuleErrors.ModuleNotFound;
      }
    } else {
      throw ConnectedWallErrors.ConnectedWallNotFound;
    }
  }

  public async addConnectedWallModule(
    connectedWallId: string,
    module: Module
  ): Promise<ConnectedWall> {
    const connectedWallDoc = this.connectedWallCollection.doc(connectedWallId);

    const connectedWallRef = await connectedWallDoc.get();

    if (connectedWallRef.exists) {
      const connectedWall = connectedWallRef.data()!;

      const newModule: Module = { ...module, id: uuid.v1() };

      connectedWall.modules.push(newModule);

      await connectedWallDoc.update({ modules: connectedWall.modules });

      return connectedWall;
    } else {
      throw ConnectedWallErrors.ConnectedWallNotFound;
    }
  }

  public async removeConnectedWallModule(
    connectedWallId: string,
    moduleId: string
  ): Promise<ConnectedWall> {
    const connectedWallDoc = this.connectedWallCollection.doc(connectedWallId);

    const connectedWallRef = await connectedWallDoc.get();

    if (connectedWallRef.exists) {
      const connectedWall = connectedWallRef.data()!;

      const moduleIndex = connectedWall.modules.findIndex(
        (m) => m.id === moduleId
      );

      if (moduleIndex > -1) {
        connectedWall.modules.splice(moduleIndex, 1);

        await connectedWallDoc.update({ modules: connectedWall.modules });

        return connectedWall;
      } else {
        throw ModuleErrors.ModuleNotFound;
      }
    } else {
      throw ConnectedWallErrors.ConnectedWallNotFound;
    }
  }

  public async deleteConnectedWall(connectedWallId: string): Promise<boolean> {
    const connectedWallRef = this.connectedWallCollection.doc(connectedWallId);

    await connectedWallRef.delete();

    return !(await connectedWallRef.get()).exists;
  }
}
