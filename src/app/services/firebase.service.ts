import { Injectable } from '@angular/core';
import Firebase from 'firebase';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  private _app: Firebase.app.App;

  constructor() {
    this._app = Firebase.initializeApp({
      apiKey: environment.apiKey,
      appId: environment.appId,
      authDomain: environment.authDomain,
      databaseUrl: environment.databaseUrl,
      projectId: environment.projectId,
      storageBocket: environment.storageBucket,
      messagingSenderId: environment.messagingSenderId,
      measumentId: environment.measurementId,
    });
  }

  public get firestore(): Firebase.firestore.Firestore {
    return this._app.firestore();
  }
}
