import { TestBed } from '@angular/core/testing';

import { ConnectedWallsService } from './connectedWalls.service';

describe('ConnectedWallsService', () => {
  let service: ConnectedWallsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectedWallsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
