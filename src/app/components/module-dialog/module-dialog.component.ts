import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Module, ModuleType } from 'src/models/module';

import { ModuleComponent } from '../module/module.component';

interface DialogDatas {
  module: Module;
  onAccept: (
    newContent: string,
    newType: ModuleType,
    newStyles: string
  ) => void;
}

@Component({
  selector: 'app-module-dialog',
  templateUrl: './module-dialog.component.html',
  styleUrls: ['./module-dialog.component.scss'],
})
export class ModuleDialogComponent implements OnInit {
  public ModuleType = ModuleType;
  public moduleTypesArray = Object.values(ModuleType)
    .map((v, i) => (typeof v === 'string' ? { value: v, index: i } : undefined))
    .filter((x) => x !== undefined) as { value: string; index: number }[];

  public content: string;
  public additionnalStyles: string;
  public moduleType: ModuleType;

  private _onAccept: (
    newContent: string,
    newType: ModuleType,
    newStyles: string
  ) => void;

  constructor(
    private dialogRef: MatDialogRef<ModuleComponent>,
    @Inject(MAT_DIALOG_DATA) data: DialogDatas
  ) {
    this.content = data.module.content ?? '';
    this.moduleType = data.module.type ?? ModuleType.Text;
    this.additionnalStyles = data.module.additionnalStyles ?? '';

    this._onAccept = data.onAccept;
  }

  ngOnInit(): void {}

  public close() {
    this.dialogRef.close();
  }

  public save() {
    if (this._onAccept) {
      this._onAccept(this.content, this.moduleType, this.additionnalStyles);
    }

    this.dialogRef.close();
  }
}
