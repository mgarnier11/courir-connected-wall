import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedWallPageComponent } from './connected-wall-page.component';

describe('ConnectedWallPageComponent', () => {
  let component: ConnectedWallPageComponent;
  let fixture: ComponentFixture<ConnectedWallPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectedWallPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedWallPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
