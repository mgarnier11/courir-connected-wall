import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConnectedWallsService } from 'src/app/services/connectedWalls.service';
import { ConnectedWall } from 'src/models/connectedWall';

@Component({
  selector: 'app-connected-wall-page',
  templateUrl: './connected-wall-page.component.html',
  styleUrls: ['./connected-wall-page.component.scss'],
})
export class ConnectedWallPageComponent implements OnInit {
  public connectedWallId: string;
  public connectedWall: ConnectedWall;

  private _sub: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _connectedWallsService: ConnectedWallsService
  ) {}

  ngOnInit(): void {
    this._sub = this._route.params.subscribe(async (params) => {
      try {
        if (params['id']) {
          this.connectedWallId = params['id'];

          this.connectedWall = await this._connectedWallsService.getConnectedWall(
            this.connectedWallId
          );
        }
      } catch (error) {
        this._router.navigate(['/']);
      }
    });
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }
}
