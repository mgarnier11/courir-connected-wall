import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedWallComponent } from './connected-wall.component';

describe('ConnectedWallComponent', () => {
  let component: ConnectedWallComponent;
  let fixture: ComponentFixture<ConnectedWallComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectedWallComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedWallComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
