import Firebase from 'firebase';

import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ConnectedWallsService } from 'src/app/services/connectedWalls.service';
import { ConnectedWall } from 'src/models/connectedWall';
import { Module } from 'src/models/module';

@Component({
  selector: 'app-connected-wall',
  templateUrl: './connected-wall.component.html',
  styleUrls: ['./connected-wall.component.scss'],
})
export class ConnectedWallComponent implements OnInit, OnDestroy {
  @Input('connectedWall') public connectedWall: ConnectedWall;
  @Input('enabled') public enabled: boolean;

  private _unsubscribe: () => void;

  constructor(private _connectedWallsService: ConnectedWallsService) {
    this._onConnectedWallUpdate = this._onConnectedWallUpdate.bind(this);
    this._onError = this._onError.bind(this);
  }

  ngOnInit(): void {
    if (!this.connectedWall) {
      throw new Error('ConnectedWall attribute is required');
    }

    this._unsubscribe = this._connectedWallsService.subscribeToConnectedWallChanges(
      this.connectedWall.id,
      this._onConnectedWallUpdate,
      this._onError
    );

    // setInterval(() => {
    //   this._connectedWallsService.updateConnectedWallName(
    //     this.connectedWall.id,
    //     this.connectedWall.name + this.connectedWall.name.length
    //   );
    // }, 50000);
  }

  private _onConnectedWallUpdate(newConnectedWall: ConnectedWall) {
    this.connectedWall = newConnectedWall;

    console.log(newConnectedWall);
  }

  private _onError(error?: Firebase.firestore.FirestoreError) {
    console.error(error);
  }

  public moduleChanged(newModule: Module) {
    if (this.enabled) {
      this._connectedWallsService.updateConnectedWallModule(
        this.connectedWall.id,
        newModule
      );
    }
  }

  public moduleDeleted(deletedModuleId: string) {
    if (this.enabled) {
      this._connectedWallsService.removeConnectedWallModule(
        this.connectedWall.id,
        deletedModuleId
      );
    }
  }

  ngOnDestroy() {
    if (this._unsubscribe) this._unsubscribe();
  }
}
