import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { ConnectedWallDialogComponent } from 'src/app/connected-wall-dialog/connected-wall-dialog.component';
import { ConnectedWallsService } from 'src/app/services/connectedWalls.service';
import { ConnectedWall } from 'src/models/connectedWall';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  public connectedWallsList: ConnectedWall[] = [];
  constructor(
    private _router: Router,
    private _connectedWallsService: ConnectedWallsService,
    private _dialog: MatDialog
  ) {
    this.refreshList();

    this._onCreateConnectedWall = this._onCreateConnectedWall.bind(this);
  }

  public refreshList() {
    this._connectedWallsService
      .getConnectedWalls()
      .then((connectedWalls) => (this.connectedWallsList = connectedWalls));
  }

  ngOnInit(): void {}

  public createNewWall() {
    this._dialog.open(ConnectedWallDialogComponent, {
      data: {
        connectedWall: { name: '' },
        onAccept: this._onCreateConnectedWall,
      },
    });
  }

  public async deleteWall(connectedWallId: string) {
    await this._connectedWallsService.deleteConnectedWall(connectedWallId);

    this.refreshList();
  }

  private async _onCreateConnectedWall(name: string) {
    const wall = await this._connectedWallsService.createConnectedWall(
      [],
      name
    );

    this._router.navigate(['modify-wall', wall.id]);
  }
}
