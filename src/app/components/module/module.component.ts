import { CdkDragEnd } from '@angular/cdk/drag-drop';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ResizeEvent } from 'angular-resizable-element';
import { Module, ModuleType } from 'src/models/module';
import { DomSanitizer } from '@angular/platform-browser';
import { ModuleDialogComponent } from '../module-dialog/module-dialog.component';
import * as ytbIframeApiLoader from 'youtube-iframe';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.scss'],
})
export class ModuleComponent implements OnInit, AfterViewInit {
  @ViewChild('moduleElement', { static: false })
  public moduleElement: ElementRef<HTMLDivElement>;
  @ViewChild('ytbIframe', { static: false })
  public ytbIframeElement: ElementRef<HTMLDivElement>;
  @Input('module') public module: Module;
  @Input('enabled') public enabled: boolean = false;
  @Output('moduleDeleted') public moduleDeleted = new EventEmitter<string>();
  @Output('moduleChanged') public moduleChanged = new EventEmitter<Module>();

  private _player: any;

  public clientWidth = 0;
  public clientHeight = 0;

  public ModuleType = ModuleType;

  public ddDisabled = true;

  public get styles() {
    // console.log(this.moduleElement);

    // console.log(this.moduleElement.nativeElement.parentElement?.clientWidth);

    return {
      width: `${this.module.size.width}%`,
      height: `${this.module.size.height}%`,
      top: `${this.module.position.y}%`,
      left: `${this.module.position.x}%`,
    };
  }

  public get contentStyles() {
    return { border: this.enabled ? '1px solid lightgray' : '' };
  }

  private get _videoId() {
    return this.module.content.match(this._regexYtb)![1] ?? this.module.content;
  }

  constructor(private _dialog: MatDialog, private _sanitizer: DomSanitizer) {
    this._onSaveModule = this._onSaveModule.bind(this);
  }

  ngOnInit(): void {
    this.ddDisabled = !this.enabled;

    if (!this.module) {
      throw new Error('Module attribute is required');
    }
  }

  ngAfterViewInit() {
    this.clientHeight =
      this.moduleElement.nativeElement.parentElement?.parentElement
        ?.clientHeight ?? 0;
    this.clientWidth =
      this.moduleElement.nativeElement.parentElement?.parentElement
        ?.clientWidth ?? 0;

    if (this.isYTBUrl(this.module.content)) {
      ytbIframeApiLoader.load((API) => {
        this._player = new API.Player(this.ytbIframeElement.nativeElement, {
          width: (this.module.size.width * this.clientWidth) / 100,
          height: (this.module.size.height * this.clientHeight) / 100,
          videoId: this._videoId,
          playerVars: {
            autoplay: 1,
            controls: 0,
            loop: 1,
            showinfo: 0,
          },
        });
      });
    }
  }

  public onResizeStart() {
    this.ddDisabled = true;
  }

  public onResizeEnd(event: ResizeEvent) {
    this.ddDisabled = !this.enabled;

    console.log(event);

    // let newWidth = this.module.size.width;

    // const newPos = this.module.position;

    // if (event.edges.left) {
    //   const l = (event.edges.left as number) * 100 / this.clientWidth;

    //   newWidth = this.module.size.width - l;

    //   newPos.x = newPos.x + l;

    // } else if (event.edges.right) {

    // }

    let newWidth = this.module.size.width;

    const newPos = this.module.position;

    if (event.edges.left) {
      const w = ((event.edges.left as number) * 100) / this.clientWidth;

      newWidth = this.module.size.width - w;

      newPos.x = newPos.x + w;
    } else if (event.edges.right) {
      newWidth =
        this.module.size.width +
        ((event.edges.right as number) * 100) / this.clientWidth;
    }

    let newHeight = this.module.size.height;

    if (event.edges.top) {
      const h = ((event.edges.top as number) * 100) / this.clientHeight;

      newHeight = this.module.size.height - h;

      console.log(h, this.module.size, this.module.position);

      newPos.y = newPos.y + h;
    } else if (event.edges.bottom) {
      newHeight =
        this.module.size.height +
        ((event.edges.bottom as number) * 100) / this.clientHeight;
    }

    const newSize = this.checkModuleSize({
      width: newWidth,
      height: newHeight,
    });

    this.module.size = newSize;

    this.moduleChanged.emit({
      ...this.module,
      size: newSize,
      position: newPos,
    });
  }

  public onDragEnd(event: CdkDragEnd) {
    console.log(event);

    let newXPercent =
      this.module.position.x + (event.distance.x * 100) / this.clientWidth;

    let newYPercent =
      this.module.position.y + (event.distance.y * 100) / this.clientHeight;

    console.log(
      newXPercent,
      this.module.position.x,
      (event.distance.x * 100) / this.clientWidth,
      this.clientWidth
    );

    this.moduleChanged.emit({
      ...this.module,
      position: this.checkModulePos({ y: newYPercent, x: newXPercent }),
    });
  }

  public checkModuleSize(size: {
    width: number;
    height: number;
  }): { width: number; height: number } {
    let newWidthPercent = size.width;
    let newHeightPercent = size.height;

    const newWidthPx = (newWidthPercent * this.clientWidth) / 100;
    const newHeightPx = (newHeightPercent * this.clientHeight) / 100;

    if (newWidthPercent > 100 - this.module.position.x)
      newWidthPercent = 100 - this.module.position.x;
    if (newWidthPx <= 40) {
      newWidthPercent = (40 * 100) / this.clientWidth;
    }

    if (newHeightPercent > 100 - this.module.position.y)
      newHeightPercent = 100 - this.module.position.y;
    if (newHeightPx <= 40) {
      newHeightPercent = (40 * 100) / this.clientHeight;
    }

    return { width: newWidthPercent, height: newHeightPercent };
  }

  public checkModulePos(pos: {
    x: number;
    y: number;
  }): { x: number; y: number } {
    let newY = pos.y;
    let newX = pos.x;

    if (newY < 0) newY = 0;
    if (newY > 100 - this.module.size.height)
      newY = 100 - this.module.size.height;

    if (newX < 0) newX = 0;
    if (newX > 100 - this.module.size.width)
      newX = 100 - this.module.size.width;

    console.log(pos, { x: newX, y: newY });

    return { y: newY, x: newX };
  }

  public openModuleDialog() {
    this._dialog.open(ModuleDialogComponent, {
      data: {
        module: this.module,
        onAccept: this._onSaveModule,
      },
    });
  }

  public deleteModule() {
    this.moduleDeleted.emit(this.module.id);
  }

  private _regexYtb = /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;

  public isYTBUrl(url: string): boolean {
    if (url.match(this._regexYtb)) {
      return true;
    }
    return false;
  }

  public videoUrl() {
    const videoId = this.module.content.match(this._regexYtb)![1];

    if (videoId) {
      return this._sanitizer.bypassSecurityTrustResourceUrl(
        `https://www.youtube.com/embed/${videoId}`
      );
    } else {
      return this._sanitizer.bypassSecurityTrustResourceUrl(
        this.module.content + '?autoplay=1'
      );
    }
  }

  private _onSaveModule(
    newContent: string,
    newType: ModuleType,
    newStyle: string
  ) {
    this.moduleChanged.emit({
      ...this.module,
      content: newContent,
      type: newType,
      additionnalStyles: newStyle,
    });
  }
}
