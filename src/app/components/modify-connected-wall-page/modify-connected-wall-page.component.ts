import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { ConnectedWallDialogComponent } from 'src/app/connected-wall-dialog/connected-wall-dialog.component';
import { ConnectedWallsService } from 'src/app/services/connectedWalls.service';
import { ConnectedWall } from 'src/models/connectedWall';
import { ModuleType } from 'src/models/module';

@Component({
  selector: 'app-modify-connected-wall-page',
  templateUrl: './modify-connected-wall-page.component.html',
  styleUrls: ['./modify-connected-wall-page.component.scss'],
})
export class ModifyConnectedWallPageComponent implements OnInit, OnDestroy {
  public connectedWallId: string;
  public connectedWall: ConnectedWall;

  private _sub: Subscription;

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _dialog: MatDialog,
    private _connectedWallsService: ConnectedWallsService
  ) {
    this._onSaveConnectedWall = this._onSaveConnectedWall.bind(this);
  }

  ngOnInit(): void {
    this._sub = this._route.params.subscribe(async (params) => {
      try {
        if (params['id']) {
          this.connectedWallId = params['id'];

          this.connectedWall = await this._connectedWallsService.getConnectedWall(
            this.connectedWallId
          );
        }
      } catch (error) {
        this._router.navigate(['/']);
      }
    });
  }

  ngOnDestroy() {
    this._sub.unsubscribe();
  }

  public addModule() {
    this._connectedWallsService.addConnectedWallModule(this.connectedWall.id, {
      content: '',
      position: { x: 50, y: 50 },
      size: { height: 10, width: 10 },
      type: ModuleType.Text,
      id: '',
      additionnalStyles: '',
    });
  }

  public editName() {
    this._dialog.open(ConnectedWallDialogComponent, {
      data: {
        connectedWall: this.connectedWall,
        onAccept: this._onSaveConnectedWall,
      },
    });
  }

  public back() {
    this._router.navigate(['/']);
  }

  private _onSaveConnectedWall(newName: string) {
    console.log(newName);

    this._connectedWallsService.updateConnectedWallName(
      this.connectedWall.id,
      newName
    );
  }
}
