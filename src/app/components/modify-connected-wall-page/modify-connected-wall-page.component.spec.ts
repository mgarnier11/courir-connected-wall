import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModifyConnectedWallPageComponent } from './modify-connected-wall-page.component';

describe('ModifyConnectedWallPageComponent', () => {
  let component: ModifyConnectedWallPageComponent;
  let fixture: ComponentFixture<ModifyConnectedWallPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModifyConnectedWallPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModifyConnectedWallPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
