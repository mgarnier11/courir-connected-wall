import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConnectedWall } from 'src/models/connectedWall';
import { ModifyConnectedWallPageComponent } from '../components/modify-connected-wall-page/modify-connected-wall-page.component';

interface DialogDatas {
  connectedWall: ConnectedWall;
  onAccept: (newName: string) => void;
}

@Component({
  selector: 'app-connected-wall-dialog',
  templateUrl: './connected-wall-dialog.component.html',
  styleUrls: ['./connected-wall-dialog.component.scss'],
})
export class ConnectedWallDialogComponent implements OnInit {
  public name: string;

  private _onAccept: (newName: string) => void;

  constructor(
    private dialogRef: MatDialogRef<ModifyConnectedWallPageComponent>,
    @Inject(MAT_DIALOG_DATA) data: DialogDatas
  ) {
    this.name = data.connectedWall.name ?? '';

    this._onAccept = data.onAccept;
  }

  ngOnInit(): void {}

  public close() {
    this.dialogRef.close();
  }

  public save() {
    if (this._onAccept) {
      this._onAccept(this.name);
    }

    this.dialogRef.close();
  }
}
