import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConnectedWallDialogComponent } from './connected-wall-dialog.component';

describe('ConnectedWallDialogComponent', () => {
  let component: ConnectedWallDialogComponent;
  let fixture: ComponentFixture<ConnectedWallDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConnectedWallDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConnectedWallDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
