import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConnectedWallPageComponent } from './components/connected-wall-page/connected-wall-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ModifyConnectedWallPageComponent } from './components/modify-connected-wall-page/modify-connected-wall-page.component';

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'wall/:id', component: ConnectedWallPageComponent },
  { path: 'modify-wall/:id', component: ModifyConnectedWallPageComponent },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
