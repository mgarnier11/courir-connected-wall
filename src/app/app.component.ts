import {
  CdkDragDrop,
  CdkDragEnd,
  CdkDragMove,
  moveItemInArray,
} from '@angular/cdk/drag-drop';
import { Component, ElementRef, ViewChild } from '@angular/core';
import { ConnectedWall } from 'src/models/connectedWall';
import { ModuleType } from 'src/models/module';
import { ConnectedWallsService } from './services/connectedWalls.service';
import { FirebaseService } from './services/firebase.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  //@ts-ignore
  @ViewChild('draggeditem') patate: ElementRef;

  /**
   *
   */
  constructor(private _connectedWallsService: ConnectedWallsService) {}

  dropped(e: CdkDragEnd) {
    console.log(e.source.getFreeDragPosition());

    // console.log(e.source.get);

    console.log(e);
  }

  moved(e: CdkDragMove) {
    console.log(e);
  }

  ended(e: CdkDragDrop<any>) {
    console.log(e);
  }
}
