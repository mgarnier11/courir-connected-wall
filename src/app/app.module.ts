import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatTooltipModule } from '@angular/material/tooltip';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ResizableModule } from 'angular-resizable-element';

import { ConnectedWallComponent } from './components/connected-wall/connected-wall.component';

import { ModuleComponent } from './components/module/module.component';
import { ModifyConnectedWallPageComponent } from './components/modify-connected-wall-page/modify-connected-wall-page.component';
import { ConnectedWallPageComponent } from './components/connected-wall-page/connected-wall-page.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { ModuleDialogComponent } from './components/module-dialog/module-dialog.component';
import { ConnectedWallDialogComponent } from './connected-wall-dialog/connected-wall-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    ModuleComponent,
    HomePageComponent,
    ConnectedWallComponent,
    ModifyConnectedWallPageComponent,
    ConnectedWallPageComponent,
    HomePageComponent,
    ModuleDialogComponent,
    ConnectedWallDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    DragDropModule,
    MatButtonModule,
    ResizableModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatTableModule,
    MatTooltipModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ModuleDialogComponent, ConnectedWallDialogComponent],
})
export class AppModule {}
