export enum ModuleType {
  Photo,
  Video,
  Text,
}

export enum ModuleErrors {
  ModuleNotFound,
}

export interface Module {
  size: {
    width: number;
    height: number;
  };
  position: {
    x: number;
    y: number;
  };
  type: ModuleType;
  content: string;
  id: string;
  additionnalStyles: string;
}
