import Firebase from 'firebase';

import { Module } from './module';

export enum ConnectedWallErrors {
  ConnectedWallNotFound,
}

export interface ConnectedWall {
  modules: Module[];
  name: string;
  id: string;
}

export const FirestoreConnectedWallConverter: Firebase.firestore.FirestoreDataConverter<ConnectedWall> = {
  toFirestore: (
    connectedWall: ConnectedWall
  ): Firebase.firestore.DocumentData => ({
    modules: connectedWall.modules,
    name: connectedWall.name,
  }),
  fromFirestore: (
    snapshot: Firebase.firestore.QueryDocumentSnapshot,
    options: Firebase.firestore.DocumentData
  ) => {
    const data = snapshot.data(options)!;
    return {
      modules: data.modules,
      id: snapshot.ref.id,
      name: data.name,
    };
  },
};
