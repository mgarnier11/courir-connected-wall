// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiKey: 'AIzaSyBvShedx5kylNqlmOMl_ofzY1r_Ii8xwKw',
  authDomain: 'courir-screen.firebaseapp.com',
  projectId: 'courir-screen',
  storageBucket: 'courir-screen.appspot.com',
  messagingSenderId: '361199467342',
  appId: '1:361199467342:web:4ee455426960018d963c1f',
  measurementId: 'G-TBKSK6W4VJ',
  databaseUrl: 'courir-screen.firebaseio.com',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
